<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SET CHARACTER SET UTF8";
$conn->query($sql);

// Handling form submission
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['region_id'])) {
        $regionID = $_POST['region_id'];

        // SQL query to retrieve districts based on the region ID
        $sql = "SELECT nazev FROM okres WHERE kraj_id = '$regionID'";

        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // Output data of each row
            echo "<h3>Seznam okresů:</h3>";
            while ($row = $result->fetch_assoc()) {
                echo "Název okresu: " . $row["nazev"] . "<br>";
            }
        } else {
            echo "0 results";
        }
    } else {
        echo "Region ID not provided.";
    }
}
$conn->close();
?>