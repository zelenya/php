<?php
require 'vendor/autoload.php';

use Latte\Engine;

$vaha = str_replace(',', '.', $_GET['weight'] ?? 0);
$vyska = str_replace(',', '.', $_GET['height'] ?? 0);
 
$bmi = " ";
$bmiCategory = " ";

if ($vaha > 0 && $vyska > 0) {

    $bmi = $vaha/($vyska*$vyska);

    if ($bmi < 19) {
        $bmiCategory = 'Podváha';
    } elseif ($bmi >= 19 && $bmi < 24) {
        $bmiCategory = 'Optimální váha';
    } elseif ($bmi >= 24 && $bmi < 29) {
        $bmiCategory = 'Nadváha';
    } elseif ($bmi >= 29 && $bmi < 39) {
        $bmiCategory = 'Obezita';
    } else{
        $bmiCategory = 'Silná obezita';
    }
} else {
    echo '<script>alert("Špatné hodnoty, prosím opravte hodnoty pro správný výpočet.")</script>';
}


$latte = new Engine;
$latte->setTempDirectory('temp');  
$latte->render('template/bmiTemplate.latte', ['bmi' => $bmi, 'bmiCategory' => $bmiCategory]);
?>