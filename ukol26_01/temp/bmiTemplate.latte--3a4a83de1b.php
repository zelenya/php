<?php

use Latte\Runtime as LR;

/** source: template/bmiTemplate.latte */
final class Template3a4a83de1b extends Latte\Runtime\Template
{
	public const Source = 'template/bmiTemplate.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BMI kalkulačka</title>
</head>
<body>
    <p>Zadej svojí váhu a výšku v metrech.</p>
      <form action="bmi.php" method="get">
        <label for="weight">Váha (kg):</label>
        <input type="text" name="weight" id="weight" required>
        
        <label for="height">Výška (m):</label>
        <input type="text" name="height" id="height" required>

        <button type="submit">Vypočítat</button>

        <p>Tvoje BMI je: ';
		echo LR\Filters::escapeHtmlText($bmi) /* line 19 */;
		echo '</p>
        <p>Kategorie: ';
		echo LR\Filters::escapeHtmlText($bmiCategory) /* line 20 */;
		echo '</p>
</body>
</html>
';
	}
}
