<?php

use Latte\Runtime as LR;

/** source: template/template1.latte */
final class Template2c921fc442 extends Latte\Runtime\Template
{
	public const Source = 'template/template1.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<p>Hodnota proměnné $items2: ';
		echo LR\Filters::escapeHtmlText($items2) /* line 1 */;
		echo '</p>
<p>Hodnota proměnné $items1[0]: ';
		echo LR\Filters::escapeHtmlText($items1[0]) /* line 2 */;
		echo '</p>
';
		foreach ($items1 as $item) /* line 3 */ {
			echo '    <p>Prvek pole $items1: ';
			echo LR\Filters::escapeHtmlText($item) /* line 4 */;
			echo '</p>
';

		}

		echo "\n";
		if ($items1) /* line 7 */ {
			echo '<ul>                 
';
			foreach ($items1 as $item) /* line 8 */ {
				echo '           
	<li>';
				echo LR\Filters::escapeHtmlText($item) /* line 9 */;
				echo '</li>                
';

			}

			echo '                          
</ul>
';
		}
		echo "\n";
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['item' => '3, 8'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}
}
