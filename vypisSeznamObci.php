<?php
/* 
  Doplňte kód tak, aby zobrazil z DB seznam obcí dle id okresu. Id okresu doplňte do SQL na pevno, 
  není potřeba zadávat pomocí formuláře
*/

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SET CHARACTER SET UTF8";
$conn->query($sql);

// Replace '1' with the desired district ID
$districtID = '3';

$sql = "SELECT nazev FROM obec WHERE okres_id = '$districtID'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    echo "Název obce: " . $row["nazev"] . "<br>";
  }
} else {
  echo "0 results";
}

$conn->close();
?>
