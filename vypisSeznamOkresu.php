<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SET CHARACTER SET UTF8";
$conn->query($sql);

// Předpokládáme, že id kraje je 1 (můžete změnit podle potřeby)
$id_kraje = 1;

$sql = "SELECT nazev_okresu FROM tabulka_okresy WHERE id_kraje = $id_kraje";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while ($row = $result->fetch_assoc()) {
    echo $row["nazev_okresu"] . "<br>";
  }
} else {
  echo "0 results";
}

$conn->close();
?>
